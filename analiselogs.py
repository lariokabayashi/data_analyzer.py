from pandas import read_csv
import numpy as np
import matplotlib.pyplot as plt
log = read_csv("dv_driving_dynamics.tsv", delimiter = "/")

fig, axs = plt.subplots(4,2)

def analyze_metric(metric_name, color, m, n):
	global log

	timestamps = log["timestamp"]
	metric_value = log[metric_name]
	#convert to numpy array
	timestamps = np.asarray(timestamps)
	metric_value = np.asarray(metric_value)
	metric_min = np.min(metric_value)
	metric_max = np.max(metric_value)
	metric_mean = np.mean(metric_value)
	metric_stdev = np.std(metric_value)

	axs[m, n].plot(timestamps, metric_value, color = color, linestyle = 'solid', linewidth = 2)

	return {
	"min": metric_min,
	"max": metric_max,
	"mean": metric_mean,#media
	"stdev": metric_stdev #desvio padrao
		}

stats_speed = analyze_metric("speed_actual", '#d10826', 0, 0,) #(métrica, cor, quadrante(m,n))
print(stats_speed) #imprimindo estatísticas
axs[0, 0].set_title( "Velocidade atual × Tempo") #definindo título
axs[0, 0].set(ylabel = 'Velocidade') #defindo quem é o eixo y (o mesmo do quadrante ao lado)
axs[0, 0].get_xaxis().set_visible(False) #em todos os gráficos a divisão do eixo x é a mesma, por isso divisão dele só é visivel embaixo 

stats_speed_target = analyze_metric("speed_target", '#d10826', 0, 1)
print(stats_speed_target)
axs[0, 1].set_title( "Velocidade alvo × Tempo")
axs[0, 1].get_xaxis().set_visible(False)

steering_angle_stats = analyze_metric("steering_angle_actual", "#001e63", 1, 0)
print(steering_angle_stats)
axs[1, 0].set_title( "Steering atual × Tempo")
axs[1,0].set(ylabel = 'Steering')
axs[1,0].get_xaxis().set_visible(False)

steering_angle_target_stats = analyze_metric("steering_angle_target", "#001e63", 1, 1)
print(steering_angle_target_stats)
axs[1, 1].set_title( "Steering alvo × Tempo")
axs[1, 1].get_xaxis().set_visible(False)

stats_brake_hydr = analyze_metric("brake_hydr_actual","#010a00", 2, 0)
print(stats_brake_hydr)
axs[2, 0].set_title( "Freio hidráulico atual × Tempo")
axs[2, 0].set(ylabel = 'Freio hidráulico')
axs[2, 0].get_xaxis().set_visible(False)

stats_brake_hydr_target = analyze_metric("brake_hydr_target","#010a00", 2, 1)
print(stats_brake_hydr_target)
axs[2, 1].set_title( "Freio hidráulico alvo × Tempo")
axs[2, 1].get_xaxis().set_visible(False)

stats_motor_moment_actual = analyze_metric("motor_moment_actual","#ba23a6", 3, 0)
print(stats_motor_moment_actual)
axs[3, 0].set_title( "Motor moment atual × Tempo")
axs[3, 0].set(xlabel='Tempo')
axs[3, 0].set(ylabel = 'Motor moment')

stats_motor_moment_target = analyze_metric("motor_moment_target","#ba23a6", 3, 1)
print(stats_motor_moment_target)
axs[3, 1].set_title( "Motor moment alvo × Tempo")
axs[3 ,1].set(xlabel='Tempo')

plt.show()
